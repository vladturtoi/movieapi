<?php
/**
 * Created by PhpStorm.
 * User: vladturtoi
 * Date: 16/07/2019
 * Time: 18:46
 */

namespace App\Services;

use App\Movie;
use Doctrine\Common\Cache\ArrayCache;
use Tmdb\Model\AbstractModel;

class MovieDBApiClient
{
    const TOKEN = "f200ea93d28d03201a0e1caee1ebd3e6";

    private $repository;

    public function __construct()
    {
        $this->repository = new \Tmdb\Repository\MovieRepository(
            new \Tmdb\Client(new \Tmdb\ApiToken(self::TOKEN), [
                'cache' => [
                    'handler' => new ArrayCache()
                ]
            ])
        );
    }

    public function findById(int $id)
    {
        return $this->repository->load($id);
    }

    public function findByTitle(string $title)
    {
        // todo
    }

    public function createFavoritesList($userId, $name)
    {
        // todo
    }
}
