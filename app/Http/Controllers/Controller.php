<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function successResponse($content) {
        return response()->json([
            'success' => true,
            'content' => $content
        ], 200);
    }

    protected function notFoundResponse() {
        return response()->json([
            'success' => false,
            'error' => "Movie not found"
        ], 404);
    }

    protected function errorResponse() {
        return response()->json([
            'success' => false,
            'error' => "Movie not found"
        ], 404);
    }
}
