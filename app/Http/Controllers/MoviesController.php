<?php
/**
 * Created by PhpStorm.
 * User: vladturtoi
 * Date: 16/07/2019
 * Time: 18:58
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\MovieDBApiClient;

class MoviesController extends Controller
{
    /* @var \App\Services\MovieDBApiClient */
    private $movieDBApiClient;

    public function __construct(MovieDBApiClient $movieDBApiClient)
    {
        $this->movieDBApiClient = $movieDBApiClient;
    }

    public function search(string $query) {

    }

    public function findById(int $id) {
        try {
            $movie = $this->movieDBApiClient->findById($id);

            if (null == $movie) {
                return $this->notFoundResponse();
            }

            return $this->successResponse($movie);
        } catch (\Exception $e) {
            return $this->errorResponse();
        }
    }
}
